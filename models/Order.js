const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: true,
		ref:'User'
	},
	products: [{
		productId: {
			type: String,
			required: [true, "Product is Required"]
		},
		name: {
			type: String
		},
		quantity: {
			type: Number,
			required: [true, "Product quantity is required"]
		},
		price: {
			type: Number
		},
		subtotal: {
			type: Number
		}
	}],
	totalAmount: {
		type: Number,
		required: true
	},
	purhasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Order', orderSchema)