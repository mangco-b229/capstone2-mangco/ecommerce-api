const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    products: [{
        productId: {
            type: String,
            required: true
        },
        name: { type: String, },
        quantity: {
            type: Number,
            required: true
        },
        price: {
            type: Number
        },
        subtotal: {
            type: Number
        }
    }],
    totalAmount: { type: Number }
})

module.exports = mongoose.model('Cart', cartSchema)
