const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const auth = require('../middlewares/jwtVerify')

// User Registration
router.post('/register', (req, res) => {
	userControllers.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController))
})


// User Login
router.post('/login', (req, res) => {
	userControllers.login(req.body)
		.then(resultFromController => res.send(resultFromController))
})

// Get User Details
router.get('/:userId/details', auth.verify, (req, res) => {

	let data = {
		userId: req.params.userId,
		userToken: auth.decode(req.headers.authorization).id
	}
	userControllers.getProfile(data)
		.then(resultFromController => res.send(resultFromController))
})

// Set User to Admin
router.put('/:userId/setadmin', auth.verify, (req, res) => {
	const data = {
		reqParams: req.params,
		isAdmin: req.body.isAdmin,
		adminToken: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAdmin(data)
		.then(resultFromController => res.send(resultFromController))
})

// Get all Users (Admin)
router.get('/:userId/all', auth.verify, (req, res) => {
	const data ={
		userId: req.params.userId,
		adminToken: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.getAllUsers(data)
		.then(resultFromController => res.send(resultFromController))
})


// Change Password
router.put('/:userId/changepassword', auth.verify, (req, res ) => {
	const data = {
		userId: req.params.userId,
		userToken: auth.decode(req.headers.authorization),
		existingPassword: req.body.existingPassword,
		newPassword: req.body.newPassword
	}

	userControllers.changePassword(data)
		.then(resultFromController => res.send(resultFromController))

})


module.exports = router