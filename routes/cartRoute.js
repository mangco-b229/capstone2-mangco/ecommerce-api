const express = require('express')
const router = express.Router()
const cartControllers = require('../controllers/cartControllers')
const auth = require('../middlewares/jwtVerify')

router.get('/:userId/cart', auth.verify, (req, res) => {
	const userId = req.params.userId
	const userToken = auth.decode(req.headers.authorization).id

	cartControllers.getCartByUser({ userId, userToken })
		.then(resultFromController => res.send(resultFromController))
})

router.post('/:userId', auth.verify, (req, res) => {
	const { productId, quantity } = req.body
	const { userId } = req.params
	const userToken = auth.decode(req.headers.authorization).id

	cartControllers.addToCart({ productId, quantity, userToken, userId })
		.then(resultFromController => res.send(resultFromController))
})

router.put('/:userId/:productId', auth.verify, (req, res) => {
	const { productId, userId } = req.params
	const { quantity } = req.body
	const userToken = auth.decode(req.headers.authorization).id

	cartControllers.updateProductQuantity({ productId, quantity, userToken, userId })
		.then(resultFromController => res.send(resultFromController))
})

router.delete('/:userId/:productId', auth.verify, (req, res) => {
	const { userId, productId } = req.params
	const userToken = auth.decode(req.headers.authorization).id

	cartControllers.deleteProductFromCart({ productId, userToken, userId })
		.then(resultFromController => res.send(resultFromController))
})


module.exports = router