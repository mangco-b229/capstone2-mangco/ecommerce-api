const express = require('express')
const router = express.Router()
const productControllers = require('../controllers/productControllers')
const auth = require('../middlewares/jwtVerify')


router.post('/', (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.createProduct(data)
		.then(resultFromController => res.send(resultFromController))
})

router.get('/all', (req, res) => {
	productControllers.getAllProduct()
		.then(resultFromController => res.send(resultFromController))
})

router.get('/all/admin', (req, res) => {
	productControllers.getAllProductAdmin()
		.then(resultFromController => res.send(resultFromController))
})


router.get('/:productId', (req, res) => {
	productControllers.getProduct(req.params)
		.then(resultFromController => res.send(resultFromController))
})

router.put('/:productId', (req, res) => {
	const data = {
		product: req.body,
		id: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.updateProduct(data)
		.then(resultFromController => res.send(resultFromController))		
})

router.put('/:productId/archive', auth.verify, (req, res) => {
	const data = {
		reqParams: req.params,
		reqisActive: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin		
	}

	productControllers.archiveProduct(data)
        .then(resultFromController => res.send(resultFromController))		
})


module.exports = router
