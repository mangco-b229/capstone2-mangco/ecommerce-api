const express = require('express')
const router = express.Router()
const orderControllers = require('../controllers/orderControllers')
const auth = require('../middlewares/jwtVerify')

router.post('/:userId', auth.verify, (req, res) => {
	const { products } = req.body
	const { userId } = req.params
	const userToken = auth.decode(req.headers.authorization)

	orderControllers.createOrder({products, userId, userToken})
		.then(resultFromController => res.send(resultFromController))			
})

// Get Orders by User
router.get('/user/:userId', auth.verify, (req, res) => {
	const userId = req.params.userId
	const userToken = auth.decode(req.headers.authorization).id

	if(userId !== userToken) {
		return res.status(401).send({ message: "UnAuthorized" })
	}

	orderControllers.getOrdersByUser(userId)
		.then(resultFromController => res.send(resultFromController))		
})

// Get all Orders Admin Only
router.get('/all', auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	orderControllers.getAllOrders(isAdmin)
		.then(resultFromController => res.send(resultFromController))
})

module.exports = router