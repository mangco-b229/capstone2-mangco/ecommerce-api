const Product = require('../models/Product')
const User = require('../models/User')
const Order = require('../models/Order')


module.exports.createOrder = async (data) => {
	if(!data.products) {
		return { message: "Order is empty!"}
	} 

	if(data.userToken.id !== data.userId || data.userToken.isAdmin === true) {
		return { error: "Unauthorized"}
	}

	try {
		let totalAmount = 0;
        const products = await Promise.all(data.products.map(async product => {
            const productDetails = await Product.findOne({ _id: product.productId, isActive:true });
            if (!productDetails) {
                return { error: `Product with id ${product.productId} not found.` }
            }
            const subtotal = product.quantity * productDetails.price;
            totalAmount += subtotal;
            return {
                productId: product.productId,
               	name: productDetails.name,
                quantity: product.quantity,
                price: productDetails.price,
                subtotal
            }
        }))

        const newOrder = await Order.create({
        	userId: data.userId,
        	products,
        	totalAmount
        })

        return { message: "Order created" }
	} catch (err) {
		console.log(err)
		return { error: "failed to create order"}
	}
}

module.exports.getOrdersByUser = (userId) => {
	return Order.find({ userId }).then((orders, err) => {
		
		if(!orders) {
			return false
		} else {
			if (err) {
				console.log(err)
				return false
			} else {
				return orders
			}
		}
	})
}

module.exports.getAllOrders = async (isAdmin) => {

	if(!isAdmin) {
		return { message: "UnAuthorized" }
	}

	try {
		const orders = await Order.find({})
		return orders
	} catch (err) {
		console.error(err)
		return false
	}

}

