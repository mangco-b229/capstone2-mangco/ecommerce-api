const User = require('../models/User')
const auth = require('../middlewares/jwtVerify')
const bcrypt = require('bcrypt')

module.exports.registerUser = (reqBody, res) => {
	// check email duplicate
    return User.find({ email: reqBody.email }).then(result => {

        if(result.length > 0) {
            return false
        } else {

            let newUser = new User({
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10) 
            })
    
            return newUser.save().then((user, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        }
    })
}

// change password 
module.exports.changePassword = (reqBody) => {
    if(reqBody.userToken) {
        return User.findById(reqBody.userId).then(user => {
            if(!user) {
                return false
            } else {
                const match = bcrypt.compareSync(reqBody.existingPassword, user.password) 
                if(match) {
                    user.password = bcrypt.hashSync(reqBody.newPassword, 10)
                    return user.save().then(() => {
                        return true
                    })
                } else {
                    return false
                }
            }
        })
    } else {
        return false
    }
}


module.exports.login = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if(result == null) {
            return false
        } else {
            const match = bcrypt.compareSync(reqBody.password, result.password)
            if(!match) {
                return false
            } else {
                const accessToken = auth.createAccessToken(result)
                return {access: accessToken, result}
            }
        }
    })
}

module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        if(data.userToken !== data.userId) {
            return false
        }

        result.password = ""
        return result
    })
}

module.exports.getAllUsers = (data) => {
    return User.find({ _id: { $ne: data.userId } }).then(result => {
        if(!data.adminToken) {
            return false
        }

        return result
    })
}

module.exports.setAdmin = async (data) => {
    if(!data.adminToken) {
        return { message: "UnAuthorized"}
    }

    try {
        await User.findByIdAndUpdate(data.reqParams.userId, { isAdmin: data.isAdmin })
        return { message: `UserId ${data.reqParams.userId} privilages updated`}
    } catch (err) {
        console.log(err)
        return false
    }
}

