const Product = require('../models/Product') 
const User = require('../models/User')


// CREATE PRODUCT
module.exports.createProduct = async (reqBody) => {

	if(reqBody.isAdmin) {

		let existingProduct = await Product.findOne({name: reqBody.product.name}).collation({ locale: 'en', strength: 2 })

		if(existingProduct) {
			return { message: "Product with that name already exists"}
		} else {
			let newProduct = new Product({
                name: reqBody.product.name,
                description: reqBody.product.description,
                category: reqBody.product.category,
                price: reqBody.product.price
            })

            try {
            	await newProduct.save()
            	return { message: "Product successfuly saved to the database " }
            } catch (err) {
            	return false
            }
		}

	} else {
		return { message: "UnAuthorized"}
	}
}

// GET ALL PRODUCTS
module.exports.getAllProduct = () => {
	return Product.find({ isActive: true }).then(result => {
		return result
	})
}

// GET ALL PRODUCTS(ADMIN)
module.exports.getAllProductAdmin = () => {
	return Product.find({}).then(result => {
		return result
	})
}


// GET A PRODUCT
module.exports.getProduct = (id) => {
	return Product.findById(id.productId)
		.then(product => {
			if(!product) {
				return { error: "Product not found"}
			} else {
				return product
			}
		})
}

//UPDATE A PRODUCT
module.exports.updateProduct = async (data) => {

	if(!data.isAdmin) return { error: "UnAuthorized" }

	try {
		const product = await Product.findById(data.id.productId)
		if(!product) return { error: "Product not found"}

		const existingProduct = await Product.findOne({
			name: data.product.name,
			_id: { $ne: data.id.productId }
		}).collation({ locale: 'en', strength: 2 })

		// checks duplicate name
		if(existingProduct) return { message: "Product with that name already exists"}

		await Product.findByIdAndUpdate(data.id.productId, data.product)
		return { message: "Product updated"}
	} catch (err) {
		return false
	}
}

//ARCHIVE A PRODUCT 
module.exports.archiveProduct = async (data) => {
	if(data.isAdmin) {
		let updateActiveField = {
    		isActive : data.reqisActive.isActive
    	}

    	try {
    		await Product.findByIdAndUpdate(data.reqParams.productId, updateActiveField)
    		return true
    	} catch (err) {
    		return false
    	} 
	} else {
		return { message: "UnAuthorized" }
	}
}