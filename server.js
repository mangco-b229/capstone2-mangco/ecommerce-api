const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const PORT = process.env.PORT || 3500
const app = express()

mongoose.set('strictQuery', true)

mongoose.connect('mongodb+srv://admin:admin1234@cluster0.n5pfk4s.mongodb.net/ECommerce-API?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on("error", console.error.bind(console, "Connection Error"))

db.once("open", () => console.log("We're connected to the cloud database."));


// MIDDLEWARES
app.use(cors())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())


//ROUTES
app.use('/carts', require('./routes/cartRoute'))
app.use('/users', require('./routes/userRoute'))
app.use('/products', require('./routes/productRoute'))
app.use('/orders', require('./routes/orderRoute'))

app.listen(PORT, () => {
	console.log(`API is now online on port ${PORT}`)

})